var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var eventSchema = new Schema({
    name: {
        type: String,
        required: true
     
    },
   day: {
        type: Number,
        max: 100,
        min: 1,
        required: true
     
    },
    startTime:{
       type: Date,
       required:true,
       
   },
   endTime:{
       type: Date,
       required:false,
       
   },
   image: {
        type: String,
        required: false
    },

    description: {
        type: String,
        required: true
    },
       
    place:{
        type: String,
        required:true
    },
    
    
}
    
    );

var EventModel = mongoose.model('Event', eventSchema);
var EventSchema = eventSchema;
// make this available to our Node applications
module.exports = { EventSchema,EventModel}