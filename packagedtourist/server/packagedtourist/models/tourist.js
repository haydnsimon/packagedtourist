// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
require('mongoose-type-email');
require('mongoose-type-url');
var Person = require('./person');
var ObjectId = Schema.Types.ObjectId;


// create a schema
var touristSchema = new Schema({
    details: {
        type: Person,
        required: true
       
    },
    room: {
        type: Number,
        required: true
    },
    table: {
        type: Number,
        required: true
    },

    tours: 
        [ {type: ObjectId, ref: "Daytrip"} ]
       
}, {
    timestamps: true
}
    
    );

// the schema is useless so far
// we need to create a model using it
var Tourists = mongoose.model('Tourist', touristSchema);

// make this available to our Node applications
module.exports = Tourists;