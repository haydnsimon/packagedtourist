var mongoose = require('mongoose');
var Event = require('./event').EventSchema;
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;
var Schema = mongoose.Schema;


// create a schema
var daytripSchema = new Schema({
    general : {
      type: Event,
      required : true  
    },
    schedule: [Event],
 
    participants: 
        [ {type: Schema.Types.ObjectId, ref: "Tourist"} ],
    
    label: {
        type: String,
        default : "",
  
    },
    price:{
        type: Currency,
        required : true
    }
    
},  {
    timestamps: true
}   
    
    );

// the schema is useless so far
// we need to create a model using it
var Daytrip = mongoose.model('Daytrip', daytripSchema);

// make this available to our Node applications
module.exports = Daytrip;