// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
require('mongoose-type-email');
require('mongoose-type-url');
var ObjectId = Schema.Types.ObjectId;


// create a schema
module.exports = personSchema = new Schema({
    firstName: {
        type: String,
        required: true
       
    },
    secondName:{
        type: String,
        required:true
    },
    id:{
       type: String,
       required:true,
       unique:true  
    },   
    age: {
        type: Number,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
   facebook: {
        type: mongoose.SchemaTypes.Url,
        required: false
    },
    picture: {
        type: String,
        required: false
    },    
    email: {
        type: mongoose.SchemaTypes.Email,
        required: true
    },
    gender:{
        type: String,
        required:true
    }, // map to username
    userName:{
        type: String,
        required:true,
        unique:true
    }
}    
  );
