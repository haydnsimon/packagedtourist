// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
require('mongoose-type-email');
require('mongoose-type-url');
var Person = require('./person');

// create a schema
var staffSchema = new Schema({
    details: {
        type: Person,
        required: true
       
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

// the schema is useless so far
// we need to create a model using it
var Staff = mongoose.model('Staff', staffSchema);

// make this available to our Node applications
module.exports = Staff;
