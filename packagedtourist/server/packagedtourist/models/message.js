var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// create a schema
var messageSchema = new Schema({
    message: {
        type: String,
        required: true
    },
    picture: {
        type:String 
    },
    submitted: {
        type: Schema.Types.ObjectId, ref:"Tourist",
        required : true
    }


    },{
    timestamps: true
    }
    
    );

// the schema is useless so far
// we need to create a model using it
var Messages = mongoose.model('Message', messageSchema);

// make this available to our Node applications
module.exports = Messages;