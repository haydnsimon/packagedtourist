var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var fs = require('fs');
var cors = require('cors')
require('dotenv').config()
var config = require('./config');
var https = require('https');

mongoose.connect(config.mongoUrl);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("Connected correctly to db server");
});

var routes = require('./routes/index');
var users = require('./routes/userrouter');
var messageRouter = require('./routes/messageRouter');
var daytripRouter = require('./routes/daytripRouter');
var eventRouter = require('./routes/eventRouter');
var touristRouter = require('./routes/touristRouter');
var staffRouter = require('./routes/staffRouter');
var app = express();



// view engine setup
console.log(app.get('env'));
app.set('port', process.env.PORT || 8080);
app.set('secPort',443);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// passport config
var User = require('./models/user');
app.use(passport.initialize());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// allow cors
app.use(cors());
app.options('*', cors());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', routes);
app.use('/users', users);
app.use('/messages',messageRouter);
app.use('/daytrips',daytripRouter);
app.use('/events',eventRouter);
app.use('/tourists',touristRouter);
app.use('/staff',staffRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});

var privateKey = fs.readFileSync(process.env.PRIVATE_KEY).toString();
var certificate = fs.readFileSync(process.env.DOMAIN_KEY).toString();
var certificateauth = fs.readFileSync(process.env.CA).toString();
var opts = {key: privateKey, cert: certificate, ca: certificateauth};

https.createServer(opts,app).listen(443,function(){
console.log("LISTEN");	
});

module.exports = app;