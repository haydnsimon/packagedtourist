var mongoose = require('mongoose'),
    assert = require('assert');

var Messages = require('./models/message');
var Event = require('./models/event').EventModel;
var Tourist = require('./models/tourist');
var Staff = require('./models/staff');
var Person = require('./models/person');
var Trips = require('./models/daytrip');
var Users = require('./models/user');

// Connection URL
var url = 'mongodb://localhost:27017/packagedtourist';
mongoose.connect(url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("Connected correctly to server");
  

    var artourist =  [
    {
        details : {
            firstName: "Simon",
            secondName : "Crown",
            userName : "simoncrown",
            id: 41243333,
            age: 34,
            phone: "0546219250",
            facebook : "http://www.facebook.com",
            email: "silly@gmail.com",
            gender: "M",
            picture: "/images/pic2.jpg"
        },
        room : 12,
        table : 33
    },{
        details : {
            firstName: "Judy",
            secondName : "Crown",
            userName : "judycrown",
            id: 341444144,
            age: 34,
            phone: "0546219250",
            facebook : "http://www.facebook.com",
            email: "firebrand@gmail.com",
            gender: "F",
            picture: "/images/pic9.jpg"
        },
        room : 12,
        table : 33
    }, {
        
          details : {
           firstName: "Stewart",
            secondName : "Hall",
            userName : "stewarthall",
            id: 5636363,
            age: 34,
            phone: "05456219250",
            facebook : "http://www.facebook.com",
            email: "mjohdsnson@gmail.com",
            gender: "M",
            picture: "/images/pic10.jpg"
        },
        room : 52,
        table : 34
    }, {
        
          details : {
           firstName: "John",
            secondName : "Smith",
            userName : "johnsmith",
            id: 3213333339090,
            age: 84,
            phone: "05462192550",
            facebook : "http://www.facebook.com",
            email: "mjohnson@gdmail.com",
            gender: "M",
            picture: "/images/pic5.jpg"
        },
        room : 312,
        table : 34
    }, {
        
          details : {
           firstName: "Rock",
            secondName : "Hudson",
            userName : "rockhudson",
            id: 32133334233,
            age: 34,
            phone: "8989777777",
            facebook : "http://www.facebook.com",
            email: "mjohnson@gmail.com",
            gender: "M",
            picture: "/images/pic7.jpg"
        },
        room : 511,
        table : 114
    }, {
        
          details : {
           firstName: "Serena",
            secondName : "Hudson",
            userName : "serenahudson",
            id: 3215933333,
            age: 34,
            phone: "0546219250",
            facebook : "http://www.facebook.com",
            email: "mjohnson@gmail.com",
            gender: "F",
            picture: "/images/pic6.jpg"        
        },
        room : 511,
        table : 114
    }, {
          details : {
           firstName: "Delia",
            secondName : "Smith",
            userName : "deliasmith",
            id: 9090903,
            age: 34,
            phone: "05462192560",
            facebook : "http://www.facebook.com",
            email: "dsmith@gmail.com",
            gender: "F",
            picture: "/images/pic8.jpg"
        },
        room : 312,
        table : 53
    },{
          details : {
            firstName: "Calista",
            secondName : "Flockhart",
            userName : "calistaflockhart",
            id: 59090903,
            age: 46,
            phone: "5255252560",
            facebook : "http://www.facebook.com",
            email: "clofckhart@gmail.com",
            gender: "F",
            picture: "/images/pic4.jpg"
        },
        room : 312,
        table : 53
    },{
          details : {
            firstName: "Richard",
            secondName : "Harris",
            userName : "richardharris",
            id: 5909743503,
            age: 75,
            phone: "5255252560",
            facebook : "http://www.facebook.com",
            email: "rharris@gmail.com",
            gender: "M  ",
            picture: "/images/pic1.jpg"
        },
        room : 312,
        table : 53
    }]; 
    /*
    for (var i=0;i<artourist.length;i++){
    
        
        Tourist.create(artourist[i],function(err,tourist){

            if (err){
                console.log(err);
            } else {
                console.log(tourist);
            }
        })
    };
   *//*
  var message = [
      {
           message: "Selling a ticket for the dance tomorrow - Wife has left me",
           picture: "http://www.picture",
           submitted: "5769957d99ad182c3c0fad6e"          
      },
      {
           message: "Looking for seats for the boat trip on Thursday",
           submitted: "5769957d99ad182c3c0fad70"          
      },
      {
           message: "Need a babysitter Tuesday afternoon",
           picture: "http://www.picture",
           submitted: "5769957d99ad182c3c0fad72"          
      }];
      */
      
    var staff = [
        {
          details : {
            firstName: "Barack",
            secondName : "Obama",
            userName : "barackobama",
            id: 591090903,
            age: 51,
            phone: "5255252560",
            facebook : "http://www.facebook.com",
            email: "obamat@gmail.com",
            gender: "M",
            picture: "/images/obama.jpg"
        },
       title: "Tour leader",
       description: "After the Whitehouse Barack Obama has extensive executive experience. He has worked for CrownTours for 10 years"
        },
        {
          details : {
            firstName: "David",
            secondName : "Hume",
            userName : "davidhume",
            id:  991090903,
            age: 51,
            phone: "5634555252560",
            facebook : "http://www.facebook.com",
            email: "humet@gmail.com",
            gender: "M",
            picture: "/images/hume.jpg"
        },
       title: "Chief Intellectual",
       description: "A new addition to CrownTours fresh from completing his works on History and Philosophy"
        },
        {
          details : {
            firstName: "Lucrezia",
            secondName : "Borge",
            userName : "lucreziaborge",
            id:  9910934403,
            age: 51,
            phone: "5634555252560",
            facebook : "http://www.facebook.com",
            email: "humet@gmail.com",
            gender: "F",
            picture: "/images/borgia.jpg"
        },
       title: "Chief Cook",
       description: "A veteran of CrownTours she knows exactly what to add to the food to give it that oomph"
        },
        ];
      
      
      
      var eventA = [{
            name: "Breakfast",
            startTime: "06/11/16 07:00 UTC",
            endTime: "06/11/16 09:00 UTC",
            description: "Seating at fixed places",
            day:1,
            place: "Breakfast Room"
    },{
            name: "Breakfast",
            startTime: "06/11/16 07:00 UTC",
            endTime: "06/11/16 09:00 UTC",
            description: "Seating at fixed places",
            place: "Breakfast Room",
            day:2
    },{
            name: "Breakfast",
            startTime: "06/12/16 07:00 UTC",
            endTime: "06/12/16 09:00 UTC",
            description: "Seating at fixed places",
            day:3,
            place: "Breakfast Room"
    },{
            name: "Breakfast",
            startTime: "06/13/16 07:00 UTC",
            endTime: "06/13/16 09:00 UTC",
            description: "Seating at fixed places",
            day:4,
            place: "Breakfast Room"
    },{
            name: "Breakfast",
            startTime: "06/14/16 07:00 UTC",
            endTime: "06/14/16 09:00 UTC",
            description: "Seating at fixed places",
            day:5,
            place: "Breakfast Room"
    },{
            name: "Breakfast",
            startTime: "06/15/16 07:00 UTC",
            endTime: "06/15/16 09:00 UTC",
            description: "Seating at fixed places",
            day:6,place: "Breakfast Room"
    },{
            name: "Breakfast",
            startTime: "06/16/16 07:00 UTC",
            endTime: "06/16/16 09:00 UTC",
            description: "Seating at fixed places",
            day:7,place: "Breakfast Room"
    },{
            name: "Breakfast",
            startTime: "06/17/16 07:00 UTC",
            endTime: "06/17/16 09:00 UTC",
            description: "Seating at fixed places",
            day:8,place: "Breakfast Room"
    },{
            name: "Breakfast",
            startTime: "06/18/16 07:00 UTC",
            endTime: "06/18/16 09:00 UTC",
            description: "Seating at fixed places",
            day:9,place: "Breakfast Room"
    },{
            name: "Breakfast",
            startTime: "06/19/16 07:00 UTC",
            endTime: "06/19/16 08:00 UTC",
            description: "Seating at fixed places - Short breakfast before departure",
            day:10,place: "Luncheon Room"
    },{
            name: "Luncheon",
            startTime: "06/11/16 12:00 UTC",
            endTime: "06/11/16 14:00 UTC",
            description: "Seating at fixed places",
            day:1,place: "Luncheon Room"
    },{
            name: "Luncheon",
            startTime: "06/12/16 12:00 UTC",
            endTime: "06/12/16 14:00 UTC",
            description: "Seating at fixed places",
            day:2,place: "Luncheon Room"
    },{
            name: "Luncheon",
            startTime: "06/13/16 12:00 UTC",
            endTime: "06/13/16 14:00 UTC",
            description: "Seating at fixed places",
            day:3,place: "Luncheon Room"
    },{
            name: "Luncheon",
            startTime: "06/14/16 12:00 UTC",
            endTime: "06/14/16 14:00 UTC",
            description: "Seating at fixed places",
            day:4,place: "Luncheon Room"
    },{
            name: "Luncheon",
            startTime: "06/15/16 12:00 UTC",
            endTime: "06/15/16 14:00 UTC",
            description: "Seating at fixed places",
            day:5,place: "Luncheon Room"
    },{
            name: "Luncheon",
            startTime: "06/16/16 12:00 UTC",
            endTime: "06/16/16 14:00 UTC",
            description: "Seating at fixed places",
            day:6,place: "Luncheon Room"
    },{
            name: "Luncheon",
            startTime: "06/17/16 12:00 UTC",
            endTime: "06/17/16 14:00 UTC",
            description: "Seating at fixed places",
            day:7,place: "Luncheon Room"
    },{
            name: "Luncheon",
            startTime: "06/18/16 12:00 UTC",
            endTime: "06/18/16 14:00 UTC",
            description: "Seating at fixed places",
            day:8,place: "Luncheon Room"
    },
	{
            name: "Luncheon",
            startTime: "06/19/16 19:00 UTC",
            endTime: "06/19/16 21:00 UTC",
            description: "Free seating",
            day:9,place: "Outside"
    },{
            name: "Dinner",
            startTime: "06/12/16 19:00 UTC",
            endTime: "06/12/16 21:00 UTC",
            description: "Seating at fixed places",
            day:2,place: "Dinner Room"
    },{
            name: "Dinner",
            startTime: "06/13/16 19:00 UTC",
            endTime: "06/13/16 21:00 UTC",
            description: "Seating at fixed places",
            day:3,place: "Dinner Room"
    },{
            name: "Dinner",
            startTime: "06/14/16 19:00 UTC",
            endTime: "06/14/16 21:00 UTC",
            description: "Seating at fixed places",
            day:4,place: "Dinner Room"
    },{
            name: "Dinner",
            startTime: "06/15/16 19:00 UTC",
            endTime: "06/15/16 21:00 UTC",
            description: "Seating at fixed places",
            day:5,place: "Dinner Room"
    },{
            name: "Dinner",
            startTime: "06/16/16 19:00 UTC",
            endTime: "06/16/16 21:00 UTC",
            description: "Seating at fixed places",
            day:6,place: "Dinner Room"
    },{
            name: "Dinner",
            startTime: "06/17/16 19:00 UTC",
            endTime: "06/17/16 21:00 UTC",
            description: "Seating at fixed places",
            day:7,place: "Dinner Room"
    },{
            name: "Dinner",
            startTime: "06/18/16 19:00 UTC",
            endTime: "06/18/16 21:00 UTC",
            description: "Seating at fixed places",
            day:8,place: "Dinner Room"
    },{
            name: "Childrens time",
            startTime: "06/12/16 14:00 UTC",
            endTime: "06/12/16 15:30 UTC",
            description: "Fun with Crusty the Clown",
            day:2,place: "Lower Conference Room"
    },{
            name: "Childrens time",
            startTime: "06/13/16 14:00 UTC",
            endTime: "06/13/16 15:30 UTC",
            description: "Stories from Zippi",
            day:3,place: "Lower Conference Room"
    },{
            name: "Childrens time",
            startTime: "06/14/16 13:00 UTC",
            endTime: "06/14/16 17:30 UTC",
            description: "Visit to the Circus",
            day:4,place: "Special visit to the Circus"
    },{
            name: "Childrens time",
            startTime: "06/14/16 13:00 UTC",
            endTime: "06/15/16 17:30 UTC",
            description: "Art and Craft",
            day:5,place: "Lower conference room"
    }];
    
    var DayTrips = [{
         general : {
            name: "The Vine Yards around Rome",
            startTime: "06/12/16 09:00 UTC",
            endTime: "06/12/16 17:00 UTC",
            description: "Beautiful scenery and great wine tasting",
            image: "/images/trip1.jpg",
            day:2,place: "Rome"
           
        },
        schedule: [{
            name: "Vineyard and Wine Cellars Frascati",
            startTime: "06/12/16 10:00 UTC",
            description: "Famous wine cellar",
            day:2,place: "Frascati"
    },{
            name: "Castle and Wines at Tuscanny",
            startTime: "06/12/16 12:00 UTC",
            description: "Famous castles and wine",
            day:2,place: "Tuscanny"
    },{
            name: "More wine at the famous pub",
            startTime: "06/12/16 15:00 UTC",
            description: "Visit to the Visonti pub",
            day:2,place: "Visconti"
    }],
   participants: 
        [  ],
    
    label: "Essential", 
    price: 97
        
    }
    ,{
         general : {
            name: "Ecclesiatical",
            startTime: "06/13/16 09:00 UTC",
            endTime: "06/13/16 17:00 UTC",
            description: "The great churches",
            image: "/images/trip2.jpg",
            day:3,place: "Rome"
           
        },
        schedule: [{
            name: "St Marks",
            startTime: "06/13/16 10:00 UTC",
            description: "Cathedral",
            day:3,place: "St Marks"
    },{
            name: "St Peter",
            startTime: "06/13/16 12:00 UTC",
            description: "See the Pope pray",
            day:3,place: "St Peter"
    },{
            name: "The Lateran",
            startTime: "06/13/16 15:00 UTC",
            description: "The Cathedral Church Of Rome",
            day:3,place: "St John Lateran"
    }],
 
    participants: 
        [  ],
    
    label: "Recommended", 
    price: 89
        
    }, {
         general : {
            name: "Pagan",
            startTime: "06/14/16 09:00 UTC",
            endTime: "06/14/16 17:00 UTC",
            description: "Pagan Rome",
            image: "/images/trip4.jpg",
            day:4,place: "Rome"
           
        },
        schedule: [{
            name: "Colloseum",
            startTime: "06/14/16 10:00 UTC",
            description: "Special fight for today",
            day:4,place: "Central Rome"
    },{
            name: "Hippodrome",
            startTime: "06/14/16 12:00 UTC",
            description: "Bet on your horse",
            day:4,place: "Central Rome"
    },{
            name: "Temple of Venus",
            startTime: "06/14/16 15:00 UTC",
            description: "Godess of love",
            day:4,place: "Godess of Love"
    }],
 
    participants: 
        [  ],
    
    label: "Special price", 
    price: 89
        
    }];  
   
   AddCollection(Tourist,artourist)
  // AddCollection(Messages,message);
   AddCollection(Staff,staff);
   AddCollection(Event,eventA);
   AddCollection(Trips,DayTrips);

    function  AddCollection(schema,arr){
    for (var i=0;i<arr.length;i++){
       schema.create(arr[i],function(err,tourist){

            if (err){
                console.log(err);
            } else {
                console.log(tourist);
              
            }
        })
    };
    }
/*
  UpdatePic('5769993bb1d136201dfcf58a',"/images/obama.jpg");
  UpdatePic('5769993bb1d136201dfcf58c',"/images/hume.jpg");
  UpdatePic('5769993bb1d136201dfcf58e',"/images/borgia.jpg");
  */ 
/* 
  UpdatePic('5769957d99ad182c3c0fad74',"/images/pic4.jpg");
 UpdatePic('5769957d99ad182c3c0fad76',"/images/pic5.jpg"); 
  UpdatePic('5769957d99ad182c3c0fad78',"/images/pic6.jpg");
 UpdatePic('5769957d99ad182c3c0fad7a',"/images/pic7.jpg"); 
  UpdatePic('5769957d99ad182c3c0fad7c',"/images/pic8.jpg");
UpdatePic('5769957d99ad182c3c0fad7e',"/images/pic8.jpg");
*/
function UpdatePic(id,http){
    Staff.findById(id, function (err, tr) {
     if (err || tr == null) {
        console.log(err);
     }
     tr.details.picture = http;
     tr.save(function (err) {
        if (err){
          console.log(err);
          return;
        } 
        console.log("OK")
       });
    });
}
    
  
    
});

