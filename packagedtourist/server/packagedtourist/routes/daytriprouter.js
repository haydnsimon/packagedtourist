var express = require('express');
var bodyParser = require('body-parser');
var Verify = require('../middleware/verify');
var daytripRouter = express.Router();
var Daytrip = require("../models/daytrip");
var Tourist = require("../models/tourist");

module.exports = daytripRouter;

daytripRouter.use(bodyParser.json());

daytripRouter.route('/')

    /**
     * @api{get} /daytrips  Get a list of all daytrips
     * @apiName GetDayTrips
     * @apiGroup DayTrips
     * 
     * @apiSuccess {[DayTrips]} array of all the daytrips available
     */

    .get(Verify.verifyOrdinaryUser,function (req, res, next) {
        Daytrip.find({}, function (err, daytrips) {
            if (err) {
                var error = new Error("Internal error");
                error.status = 500;
                return next(error);
            } else {
                var trips = [];
                for (var i = 0; i < daytrips.length; i++) {
                    var arr = daytrips[i].participants;
                    var daytripObj = daytrips[i].toObject();
                    var arr = daytripObj.participants;
                    var userId = req.person._id;
                    daytripObj.amRegistered = false;
                    for (var j = 0; j < arr.length; j++) {
                        if (arr[j].equals(userId)) {
                            daytripObj.amRegistered = true;
                            break;
                        }
                    }
                    trips.push(daytripObj);
                }
                res.json(trips);
            }
        });
    });

/**
 * @apiName RegisterForDaytrip Register for a day trip
 * @api{put} /daytrips/register/:tripid Register for a day trip
 * @apiParam {objectID} the mongo object id requested 
 * @apiGroup DayTrips
 * @apiPermission OrdinaryUser - user ID is taken from the token
 * 
 * @apiSuccess {DayTrip} updated trip details
 * @apiError   403 already regsitered for this trip
 */
daytripRouter.route('/register/:id')

    .put(Verify.verifyOrdinaryUser, function (req, res, next) {
        Daytrip.findById(req.params.id, function (err, daytrip) {
            if (err || daytrip == null) {
                var error = new Error('Daytrip ' + req.params.DaytripId + " not found");
                error.status = 404;
                return next(error);
            }
            var arr = daytrip.participants;
            var userId = req.person._id;
            if (arr.indexOf(userId) < 0) {
                arr.push(userId);
                daytrip.participants = arr;
                daytrip.save(function (err) {
                    if (err) {
                        var error = new Error("Internal error");
                        error.status = 500;
                        return next(error);
                    }
                    // now update the user 
                    var person = req.person.toObject();
                    person.tours.push(req.params.id);

                    Tourist.findByIdAndUpdate(person._id, person, function (err, message) {
                        if (err) {
                            var error = new Error('Internal error!');
                            error.status = 500;
                            console.log(err);
                        }
                        res.json(daytrip);
                    });
                });
            } else {
                var error = new Error('Already registeed for this trip');
                error.status = 403;
                return next(error);
            }
        });
    });



daytripRouter.route('/:DaytripId')
    /**
     * @apiName  GetTripDetails
     * @api{get} /daytrips/:tripid Fetch a single trip details
     * @apiParam {objectID} the mongo object id requested 
     * @apiGroup DayTrips
     * @apiPermission OrdinaryUser - user ID is taken from the token
     * 
     * @apiSuccess {DayTrip} the daytrip
     * @apiError   403 already regsitered for this trip
     */

    .get(Verify.verifyOrdinaryUser, function (req, res, next) {
        Daytrip.findById(req.params.DaytripId).populate('participants').exec(function (err, daytrip) {
            if (err || daytrip == null) {
                var error = new Error('Daytrip ' + req.params.DaytripId + " not found");
                error.status = 404;
                return next(error);
            }
            var arr = daytrip.participants;
            var daytripObj = daytrip.toObject();
            var userId = req.person._id;
            daytripObj.amRegistered = false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i]._id.equals(userId)) {
                    daytripObj.amRegistered = true;
                    break;
                }
            }
            res.json(daytripObj);
        });
    });
