var express = require('express');
var bodyParser = require('body-parser');
var Verify = require('../middleware/verify');
var touristRouter = express.Router();
var Tourist = require("../models/tourist");
var passport = require('passport');
var User = require('../models/user');

module.exports = touristRouter;

touristRouter.use(bodyParser.json());

touristRouter.route('/')

    /**
     * @api{get} /tourists Get all tourists
     * @apiName GetAllTourists
     * @apiGroup Tourists
     * @apiPermission Administrator
     * 
     * @apiSuccess {[Tourists]} array of tourists
     */
    .get(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
        Tourist.find({}).exec(function (err, tourists) {
            if (err) {
                return next(err);
            } else {
                res.json(tourists);
            }
        });
    })

    /**
     * @api{delete} /tourists Delete all tourists
     * @apiName DeleteAllTourists
     * @apiGroup Tourists
     * @apiPermission Administrator
     * 
     * @apiSuccess {String} confirmation string
     */
    .delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
        Tourist.remove({}, function (err, removed) {
            if (err) {
                var error = new Error('Internal error!');
                error.status = 500;
                return next(error);
            }
            res.end('Deleted all tourists');
        });

    });

touristRouter.route('/:touristId')
    /**
     * @api{get} /tourists/:id Return details of a particular tourist including tours
     * @apiParam touristId the unique id to be fetched
     * @apiName FetchTouristDetails
     * @apiGroup Tourists
     * @apiPermission Administrator
     * 
     * @apiSuccess {Tourist} tourist details
     * @apiError   404 Tourist Id not found
     */
    .get(Verify.verifyOrdinaryUser, function (req, res, next) {
        Tourist.findById(req.params.touristId).populate('tours').exec(function (err, tourist) {
            if (err || tourist == null) {
                var error = new Error('Tourist ' + req.params.touristId + " not found");
                error.status = 404;
                return next(error);
            }
            res.json(tourist);
        });
    });

touristRouter.route('/register')

    .post(function (req, res, next) {
        console.log("Age is " + req.body.age);
        ;

        User.register(new User({ username: req.body.username }),
            req.body.password, function (err, user) {
                if (err) {
                    var error = new Error('Internal error!');
                    error.status = 500;
                    return next(error);
                }
                passport.authenticate('local')(req, res, function () {
                    console.log("OK");

                    var tourist = {};
                    tourist.details = {};
                    tourist.details.firstName = req.body.firstname;
                    tourist.details.secondName = req.body.secondname;
                    tourist.details.userName = req.body.username;
                    tourist.details.id = Math.floor((Math.random() * 1000) + 1);
                    tourist.details.age = req.body.age;
                    tourist.details.phone = req.body.telephone;
                    tourist.details.gender = req.body.gender;
                    tourist.details.picture = "/images/pic4.jpg";
                    tourist.details.email = req.body.email;
                    tourist.details.facebook = req.body.facebook;
                    tourist.room = req.body.room;
                    tourist.table = req.body.table

                    Tourist.create(tourist, function (err, tourist) {

                        if (err) {
                            var error = new Error('Internal error!');
                            error.status = 500;
                            return next(error);
                        } else {
                            return res.status(200).json({ status: 'Registration Successful!' });

                        }
                    });



                });
            });





    });






