var express = require('express');
var bodyParser = require('body-parser');
var Verify = require('../middleware/verify');
var staffRouter = express.Router();
var Staff = require("../models/staff");
module.exports = staffRouter;

staffRouter.use(bodyParser.json());

staffRouter.route('/')

/**
 * @api{get} /staff Get all staff
 * @apiName GetAllStaff
 * @apiGroup Staff
 * @apiPermission OrdinaryUser
 * 
 * @apiSuccess {[Staff]} array of staff
 */
.get(Verify.verifyOrdinaryUser,function(req,res,next){
    Staff.find({}).exec( function (err, staff) {
        if (err){
            return next(err);        
        } else { 
            res.json(staff);
        }
    });
})