var express = require('express');
var bodyParser = require('body-parser');
var Verify = require('../middleware/verify');
var eventRouter = express.Router();
var Event = require("../models/event").EventModel;
module.exports = eventRouter;

eventRouter.use(bodyParser.json());

eventRouter.route('/')
/**
 * @api{get} /schedule Get schedule for all days
 * @apiName GetSchedule
 * @apiGroup Schedule
 * @apiPermission Ordinary user
 * 
 * @apiSuccess {[Event]} event The event array
 */
.get(Verify.verifyOrdinaryUser,function(req,res,next){
    Event.find({}, function (err, events) {
        if (err){
           var error = new Error("Internal error");
           error.status = 500;
           return next(error);
        } else { 
            res.json(events);
        }
    });
});


eventRouter.route('/day/:dayNo')
/**
 * @api{get} /schedule/day/dayNo?nodays=n Get schedule for one or more days
 * @apiName GetScheduleForDay
 * @apiGroup Schedule
 * @apiPermission Ordinary user
 * 
 * @apiSuccess {[Event]} event The event array and the number of days that can be requested
 * @apiError    DayNotFound HTTP code 404. HTTP 500 returned if internal error
 */
.get(Verify.verifyOrdinaryUser,function(req,res,next){

    Event.find().sort({"day":-1}).limit(1).exec(
        function(err,maxEvent){
            if (err || maxEvent==null){
               var error = new Error('Internal error');
               error.status = 500;
               return next(error);
            }
            var dayFrom = parseInt(req.params.dayNo) ;
            var dayTo = dayFrom + (req.query.nodays ? parseInt(req.query.nodays) : 1);
            dayFrom--;
            Event.find().where('day').gt(dayFrom).lt(dayTo).exec(function (err, events) {
                if (err || events == null) {
                var error = new Error('Day ' + req.params.EventId + " not found");
                error.status = 404;
                return next(error);
                } 
                var objEvent = {};
                objEvent.events = events;
                objEvent.maxDay = maxEvent[0].day;
                console.log(objEvent.maxDay);
                res.json(objEvent);
            });  
      }
    );
});
