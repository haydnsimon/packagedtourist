var express = require('express');
var bodyParser = require('body-parser');
var Verify = require('../middleware/verify');
var messageRouter = express.Router();
var Message = require("../models/message");
module.exports = messageRouter;

messageRouter.use(bodyParser.json());


var isMyMessageOrAdmin = function(req,res,next){
    var id = req.params.messageId || req.query.messageId;
   Message.findById(id, function (err, message) {
        if (err || message == null) {
           var error = new Error('Internal error!');
           error.status = 500;
           return next(error);
        } 
        if (message.submitted.equals(req.person._id) || req.decoded._doc.admin){
            req.oldrecord = message;
        }
        else 
        {
           var error = new Error('Not authorized!');
           error.status = 401;
           return next(error);
        }
        next();
    });  
}


messageRouter.route('/loggedin')
.get (Verify.verifyOrdinaryUser,function(req,res,next){
    res.json(req.person);
});

/**
 * @api{get} /messages Get all the messages
 * @apiName GetMessages
 * @apiGroup Messages
 * @apiPermission Ordinary user
 * 
 * @apiSuccess {[Messages]} messages An array of all messages
 */

messageRouter.route('/')
.get(Verify.verifyOrdinaryUser,function(req,res,next){
    Message.find({}).populate('submitted').exec( function (err, messages) {
        if (err){
            return next(err);        
        } else {
            var copyArray = [];         
            for  (var i=0; i<messages.length;++i){
                var copyMessage = messages[i].toObject();
                if (copyMessage.submitted.details.userName ===  req.person.details.userName){
                    copyMessage.mine = true;
                } else {
                   copyMessage.mine = false;
                }
                copyArray.push(copyMessage);
            }
            res.json(copyArray);
        }
    });
})

/**
 * @api{post} /messages Post a message
 * @apiName PostMessage
 * @apiGroup Messages
 * @apiPermission Ordinary user
 * 
 * @apiSuccess {Messages} the message posted
 */

.post(Verify.verifyOrdinaryUser,function(req, res, next){
    var message = req.body;
    message.submitted = req.person._id;
    Message.create(message, function (err, message) {
        if (err) {
              console.log(error);
              var error = new Error('Internal error!');
              error.status = 500;
              return next(error);
        };
         res.json(message);
    });   
        
})

/**
 * @api{put} /messages Update a messsage
 * @apiName UpdateMessage
 * @apiGroup Messages
 * @apiPermission Ordinary user who submitted the message or an administrator
 * 
 * @apiSuccess {Messages} the message posted
 */

.put(Verify.verifyOrdinaryUser,isMyMessageOrAdmin,function(req, res, next){
    var newRecord = {};
    newRecord.submitted = req.oldrecord.submitted;
    newRecord.message = req.query.message,
    newRecord.picture = req.oldrecord.picture;

    Message.findByIdAndUpdate(req.query.messageId,newRecord,function(err,message){
        if (err){
             var error = new Error('Internal error!');
              error.status = 500;
              return next(error);
        }
        res.json(message);
    })
})


/**
 * @api{delete} /messages delete all messages
 * @apiName GetMessage
 * @apiGroup Messages
 * @apiPermission Administrator
 * 
 * @apiSuccess {String} confirmation string
 */
.delete(Verify.verifyOrdinaryUser,Verify.verifyAdmin,function(req, res, next){
        Message.remove({},function(err,removed){
            if (err){
              var error = new Error('Internal error!');
              error.status = 500;
              return next(error);
            }
            res.end('Deleted all messages');
        });
     
});

messageRouter.route('/:messageId')

/**
 * @api{put} /messages/:id Get a single message
 * @apiParam id the unique id to be fetched
 * @apiName GetMessage
 * @apiGroup Messages
 * @apiPermission Ordinary user
 * 
 * @apiSuccess {Message} the message requested
 */
.get(Verify.verifyOrdinaryUser,function(req,res,next){
      Message.findById(req.params.messageId).populate('submitted').exec(function (err, message) {
        if (err || message == null) {
           var error = new Error('Message ' + req.params.messageId + " not found");
           error.status = 404;
           return next(error);
        } 
        res.json(message);
    });  
})

/**
 * @api{put} /messages/:id Delete a single message
 * @apiParam id the unique id to be fetched
 * @apiName DeleteMessage
 * @apiGroup Messages
 * @apiPermission Ordinary user who submitted the message or an administrator
 * 
 * @apiSuccess {Message} the message deleted
 */

.delete(Verify.verifyOrdinaryUser,isMyMessageOrAdmin,function(req, res, next){
        Message.remove({_id : req.params.messageId},function(err,removed){
            if (err){
              var error = new Error('Internal error!');
              error.status = 500;
              return next(error);
            }
            res.json(removed);
        });
     
});



