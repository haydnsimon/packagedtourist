var User = require('../models/user');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('../config.js');
var Tourist = require('../models/tourist');
var Staff = require('../models/staff');

exports.getToken = function (user) {
    return jwt.sign(user, config.secretKey, {
        expiresIn: 36000
    });
};

exports.verifyOrdinaryUser = function (req, res, next) {
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.query.access_token ;

    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, config.secretKey, function (err, decoded) {
            if (err) {
                var err = new Error('You are not authenticated!');
                err.status = 401;
                return next(err);
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                if (!decoded._doc.admin){
                     var tourist = Tourist.findOne({'details.userName' : decoded._doc.username},function(err,person){
                         if (err || !person) { 
                             err = new Error("Server error");
                             err.status = 500;
                             return next(err);
                         };
                         req.person = person;
                         req.person.isTourist = true;
                         next();
                     }
                     
                     );
                } else {
                    var staff = Staff.findOne({'details.userName' : decoded._doc.username},function(err,person){
                         if (err || !person) { 
                             err = new Error("Server error");
                             err.status = 500;
                             return next(err);
                          };
                         req.person = person;
                         next();
                     });
                }
                
            }
        });
    } else {
        // if there is no token
        // return an error
        var err = new Error('No token provided!');
        err.status = 403;
        return next(err);
    }
};

exports.verifyAdmin = function (req, res, next) {
    if (req.decoded._doc.admin){
        // you are cleared to do this        
        next();
    } else {
        var err = new Error("You are not authorized to perform this operation");
        err.status = 403;
        return next(err);
    }      
};
