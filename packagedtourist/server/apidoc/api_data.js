define({ "api": [
  {
    "type": "get",
    "url": "/daytrips",
    "title": "Get a list of all daytrips",
    "name": "GetDayTrips",
    "group": "DayTrips",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[DayTrips]",
            "optional": false,
            "field": "array",
            "description": "<p>of all the daytrips available</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/daytriprouter.js",
    "groupTitle": "DayTrips"
  },
  {
    "name": "GetTripDetails",
    "type": "get",
    "url": "/daytrips/:tripid",
    "title": "Fetch a single trip details",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "objectID",
            "optional": false,
            "field": "the",
            "description": "<p>mongo object id requested</p>"
          }
        ]
      }
    },
    "group": "DayTrips",
    "permission": [
      {
        "name": "OrdinaryUser - user ID is taken from the token"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "DayTrip",
            "optional": false,
            "field": "the",
            "description": "<p>daytrip</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403",
            "description": "<p>already regsitered for this trip</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/daytriprouter.js",
    "groupTitle": "DayTrips"
  },
  {
    "name": "RegisterForDaytrip_Register_for_a_day_trip",
    "type": "put",
    "url": "/daytrips/register/:tripid",
    "title": "Register for a day trip",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "objectID",
            "optional": false,
            "field": "the",
            "description": "<p>mongo object id requested</p>"
          }
        ]
      }
    },
    "group": "DayTrips",
    "permission": [
      {
        "name": "OrdinaryUser - user ID is taken from the token"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "DayTrip",
            "optional": false,
            "field": "updated",
            "description": "<p>trip details</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403",
            "description": "<p>already regsitered for this trip</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/daytriprouter.js",
    "groupTitle": "DayTrips"
  },
  {
    "type": "put",
    "url": "/messages/:id",
    "title": "Delete a single message",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "id",
            "description": "<p>the unique id to be fetched</p>"
          }
        ]
      }
    },
    "name": "DeleteMessage",
    "group": "Messages",
    "permission": [
      {
        "name": "Ordinary user who submitted the message or an administrator"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Message",
            "optional": false,
            "field": "the",
            "description": "<p>message deleted</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/messagerouter.js",
    "groupTitle": "Messages"
  },
  {
    "type": "put",
    "url": "/messages/:id",
    "title": "Get a single message",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "id",
            "description": "<p>the unique id to be fetched</p>"
          }
        ]
      }
    },
    "name": "GetMessage",
    "group": "Messages",
    "permission": [
      {
        "name": "Ordinary user"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Message",
            "optional": false,
            "field": "the",
            "description": "<p>message requested</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/messagerouter.js",
    "groupTitle": "Messages"
  },
  {
    "type": "delete",
    "url": "/messages",
    "title": "delete all messages",
    "name": "GetMessage",
    "group": "Messages",
    "permission": [
      {
        "name": "Administrator"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "confirmation",
            "description": "<p>string</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/messagerouter.js",
    "groupTitle": "Messages"
  },
  {
    "type": "get",
    "url": "/messages",
    "title": "Get all the messages",
    "name": "GetMessages",
    "group": "Messages",
    "permission": [
      {
        "name": "Ordinary user"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[Messages]",
            "optional": false,
            "field": "messages",
            "description": "<p>An array of all messages</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/messagerouter.js",
    "groupTitle": "Messages"
  },
  {
    "type": "post",
    "url": "/messages",
    "title": "Post a message",
    "name": "PostMessage",
    "group": "Messages",
    "permission": [
      {
        "name": "Ordinary user"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Messages",
            "optional": false,
            "field": "the",
            "description": "<p>message posted</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/messagerouter.js",
    "groupTitle": "Messages"
  },
  {
    "type": "put",
    "url": "/messages",
    "title": "Update a messsage",
    "name": "UpdateMessage",
    "group": "Messages",
    "permission": [
      {
        "name": "Ordinary user who submitted the message or an administrator"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Messages",
            "optional": false,
            "field": "the",
            "description": "<p>message posted</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/messagerouter.js",
    "groupTitle": "Messages"
  },
  {
    "type": "get",
    "url": "/schedule",
    "title": "Get schedule for all days",
    "name": "GetSchedule",
    "group": "Schedule",
    "permission": [
      {
        "name": "Ordinary user"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[Event]",
            "optional": false,
            "field": "event",
            "description": "<p>The event array</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/eventrouter.js",
    "groupTitle": "Schedule"
  },
  {
    "type": "get",
    "url": "/schedule/day/dayNo?nodays=n",
    "title": "Get schedule for one or more days",
    "name": "GetScheduleForDay",
    "group": "Schedule",
    "permission": [
      {
        "name": "Ordinary user"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[Event]",
            "optional": false,
            "field": "event",
            "description": "<p>The event array and the number of days that can be requested</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DayNotFound",
            "description": "<p>HTTP code 404. HTTP 500 returned if internal error</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/eventrouter.js",
    "groupTitle": "Schedule"
  },
  {
    "type": "get",
    "url": "/staff",
    "title": "Get all staff",
    "name": "GetAllStaff",
    "group": "Staff",
    "permission": [
      {
        "name": "OrdinaryUser"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[Staff]",
            "optional": false,
            "field": "array",
            "description": "<p>of staff</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/staffrouter.js",
    "groupTitle": "Staff"
  },
  {
    "type": "delete",
    "url": "/tourists",
    "title": "Delete all tourists",
    "name": "DeleteAllTourists",
    "group": "Tourists",
    "permission": [
      {
        "name": "Administrator"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "confirmation",
            "description": "<p>string</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/touristrouter.js",
    "groupTitle": "Tourists"
  },
  {
    "type": "get",
    "url": "/tourists/:id",
    "title": "Return details of a particular tourist including tours",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "touristId",
            "description": "<p>the unique id to be fetched</p>"
          }
        ]
      }
    },
    "name": "FetchTouristDetails",
    "group": "Tourists",
    "permission": [
      {
        "name": "Administrator"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Tourist",
            "optional": false,
            "field": "tourist",
            "description": "<p>details</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Tourist Id not found</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/touristrouter.js",
    "groupTitle": "Tourists"
  },
  {
    "type": "get",
    "url": "/tourists",
    "title": "Get all tourists",
    "name": "GetAllTourists",
    "group": "Tourists",
    "permission": [
      {
        "name": "Administrator"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[Tourists]",
            "optional": false,
            "field": "array",
            "description": "<p>of tourists</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/touristrouter.js",
    "groupTitle": "Tourists"
  },
  {
    "type": "post",
    "url": "/users/login",
    "title": "Login",
    "name": "Login",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Token",
            "optional": false,
            "field": "Token",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/userrouter.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users/logout",
    "title": "Logout",
    "name": "Logout",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bye",
            "optional": false,
            "field": "Bye",
            "description": "<p>object</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/userrouter.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users/register",
    "title": "Register",
    "name": "RegisterUser",
    "group": "User",
    "permission": [
      {
        "name": "Administrator"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Success",
            "description": "<p>string</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "packagedtourist/routes/userrouter.js",
    "groupTitle": "User"
  }
] });
