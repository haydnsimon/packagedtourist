define({
  "name": "packaged_tourist",
  "version": "1.0.0",
  "description": "REST API for The Packaged Tourist Capstone Project",
  "title": "The Packaged Tourist",
  "url": "https://judycrown.com",
  "header": {
    "title": "The Packaged Tourist",
    "content": "<p>REST API For The Packaged Tourist</p>\n"
  },
  "footer": {
    "title": "The Packaged Tourist",
    "content": "<p>Packaged Tourist API</p>\n"
  },
  "template": {
    "withCompare": false,
    "withGenerator": false
  },
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-08-16T18:55:20.381Z",
    "url": "http://apidocjs.com",
    "version": "0.16.1"
  }
});
