angular.module('packagedtourist.daytripscontroller', [])

.controller('DaytripsCtrl', ['$ionicLoading', 'daytripsService', '$scope', 'baseURL', function($ionicLoading, daytripService, $scope, baseURL) {

    $scope.daytrips;
    $ionicLoading.show({
      template: 'Loading...'
    });
    daytripService.getTrips().query().$promise.then(
      function(trips) {
        for (var i = 0; i < trips.length; i++) {
          if (trips[i].general.image) {
            var temp = trips[i].general.image;
            trips[i].general.image = baseURL + temp;
          }
        }
        $scope.trips = trips;
        $ionicLoading.hide();
      },
      function(response) {
        console.log(response);
        $ionicLoading.hide();
      }
    );

  }])
  .controller('TourDetailsCtrl', ['$ionicPopup', '$ionicLoading', 'daytripsService', '$scope', 'baseURL', '$stateParams','$ionicPlatform', '$cordovaLocalNotification', '$cordovaToast',
   function($ionicPopup, $ionicLoading, daytripService, $scope, baseURL, $stateParams,$ionicPlatform, $cordovaLocalNotification,$cordovaToast) {
    $scope.baseURL = baseURL;
    $scope.trip = {};
    $ionicLoading.show({
      template: 'Loading...'
    });

    $scope.buyTour = function() {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Buy',
        template: 'Order this great tour. Will charge your creadit card?'
      });

      confirmPopup.then(function(res) {
        if (res) {
          daytripService.buyTrip().update({
            'id': $stateParams.id
          }, {
            'id': $stateParams.id
          }).$promise.then(
            function(response) {
              $ionicPlatform.ready(function () {
                       $cordovaLocalNotification.schedule({
                           id: 1,
                           title: "Booked",
                           text: "Enjoy your trip"
                       }).then(function () {
                           console.log('Added Favorite '+$scope.dishes[index].name);
                       },
                       function () {
                           console.log('Failed to add Notification ');
                       });

                       $cordovaToast
                         .show("Booked your trip", 'long', 'center')
                         .then(function (success) {
                             // success
                         }, function (error) {
                             // error
                         });
               });              console.log("Success");
              showMessages();
            },
            function(response) {
              console.log("Failed");
            }
          );
        }
      });
    }

    daytripService.getTrip().get({
        id: $stateParams.id
      })
      .$promise.then(
        function(response) {
          $scope.trip = response;
          console.log(response.general.name)
          $scope.showTrip = true;
          $ionicLoading.hide();
        },
        function(response) {
          console.log("failed to load image");
          $ionicLoading.hide();

        }
      );


  }]);
