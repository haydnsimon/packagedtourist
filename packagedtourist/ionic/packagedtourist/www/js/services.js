'use strict';

angular.module('packagedtourist.services', ['ngResource'])
  .constant("baseURL", "http://judycrown.com/")
  .constant('httpsURL',"https://judycrown.com/")
  .service('daytripsService', ['$resource', 'baseURL', function($resource, baseURL) {


    this.getTrips = function() {

      return $resource(baseURL + "daytrips", null, {
        'update': {
          method: 'PUT'
        }
      });

    };

    // implement a function named getPromotion
    // that returns a selected promotion.
    this.getTrip = function() {
      return $resource(baseURL + "daytrips/:id");;
    }

    this.buyTrip = function() {
      return $resource(
        baseURL + "daytrips/register/:id",
        null, {
          "update": {
            method: "PUT"
          },

        }
      );
    }


  }])
  .service('scheduleService', ['$resource', 'baseURL', function($resource, baseURL) {
    this.getScheduleForDay = function() {
      return $resource(baseURL + "events/day/:dayNo");
    }
  }])


.service('touristService', ['$resource', 'baseURL', function($resource, baseURL) {
  this.getTourist = function() {
    return $resource(baseURL + "tourists/:id");
  }

  this.getStaff = function(){
    return $resource(baseURL + "staff");
  };

}])

.service('messageService', ['tokenHandlerFactory', '$resource', 'baseURL', function(tokenHandlerFactory, $resource, baseURL) {
  var resInd = $resource(baseURL + "messages/:id", null, {
    update: {
      method: "PUT"
    }
  });

  var res = $resource(baseURL + "messages", null, {
    update: {
      method: "PUT"
    }
  });

  this.resourcePut = function() {
    return $resource(
      baseURL + "messages/:id",
      null, {
        "update": {
          method: "PUT"
        },

      }
    );
  }

  this.resourceInd = function() {
    return resInd;
  }

  this.resource = function() {
    return res;
  }

  var resTest = $resource(baseURL + "messages/loggedin");
  this.test = function() {
    return resTest;
  }
}])

.service('authService', ['$localStorage', 'messageService', 'tokenHandlerFactory', '$rootScope', function($localStorage, messageService, tokenHandlerFactory, $rootScope) {
    var isloggedin;

    this.setAuthentic = function() {
      isloggedin = true;
    }
    this.setUnAuthenticate = function() {
      isloggedin = false;
    }
    this.isAuthenticated = function() {
      console.log("Authentic is  " + isloggedin);
      return isloggedin;
    }

    this.testLogin = function(myFunc) {
      var token = $localStorage.get("access_token");
      if (token) {
        tokenHandlerFactory.set(token);
        messageService.test().get().$promise.then(
          function(response) {
            console.log(response.details.userName);
            isloggedin = true;
            $rootScope.$broadcast("userReady", response);
            myFunc(isloggedin);
          },
          function(response) {
            isloggedin = false;
            myFunc(isloggedin);
          }
        )
      } else {
        isloggedin = false;
        myFunc(isloggedin)
      }
    }
  return this;
  }])
  .factory('loginFactory', ['$resource', 'httpsURL', function($resource, httpsURL) {
    return $resource(httpsURL + "users/login");
  }])
  .factory('tokenHandlerFactory', ['$rootScope', function($rootScope) {
    return {
      set: function(value) {
        $rootScope.authToken = value;
      }
    }
  }])


.factory('$localStorage', ['$window', function($window) {
  return {
    store: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    }
  }
}]);
