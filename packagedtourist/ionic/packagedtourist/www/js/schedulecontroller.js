angular.module('packagedtourist.schedulecontroller', [])

.controller('ScheduleCtrl', ['$ionicLoading', 'scheduleService', '$scope', 'baseURL', function($ionicLoading, scheduleService, $scope, baseURL) {

    $scope.schedule = [];
    $scope.dayOptions = [];
    $scope.daySelected = 1;
    $scope.day = {};
    $ionicLoading.show({
      template: 'Loading...'
    });



    $scope.getSchedule = function(day){
      console.log(day);
      scheduleService.getScheduleForDay().get({
          dayNo: day
        })
        .$promise.then(
          function(response) {
            $scope.dayOptions=[];
            $scope.schedule=[];
            $scope.maxDay = response.maxDay;
            for (var i=1;i<=response.maxDay;i++){
              $scope.dayOptions.push(i);
            }
            for (var i=0; i<response.events.length; i++){
              if (i==0){
                $scope.day = response.events[i].startTime;
              }
              $scope.schedule.push(response.events[i]);
            }
            $ionicLoading.hide();
          },
          function(response) {
            console.log("failed to load image");
            $ionicLoading.hide();
          }
        );
    }

    $scope.getSchedule(1);

  }])
;
