// Ionic packagedtourist App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'packagedtourist' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'packagedtourist.controllers' is found in controllers.js
angular.module('packagedtourist', ['ionic',  'ngCordova', 'packagedtourist.services', 'packagedtourist.controllers','packagedtourist.daytripscontroller' ,'packagedtourist.schedulecontroller','packagedtourist.messagecontroller',
                                   'LocalStorageModule','ion-floating-menu'])
    .run(function ($ionicPlatform, $rootScope, $state, authService, $cordovaSplashscreen,$timeout) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true)
                cordova.plugins.Keyboard.disableScroll(true)
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault()
            }
        });
/*
        $rootScope.$on("unauthorized", function () {
            if (!rootScope.showingLogin){
                $scope.login();
            }
        });
*/
      $timeout(function(){
               $cordovaSplashscreen.hide();
       },5000);

    })

.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider

        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
        })
        /*
              .state('app.login', {
                url: '/login',
                views: {
                  'menuContent': {
                    templateUrl: 'templates/login.html',
                    controller : 'LoginCtrl'
                  }
                }

              })
        */
        .state('app.staff', {
            url: '/staff',
            views: {
                'menuContent': {
                    templateUrl: 'templates/staff.html',
                    controller: 'StaffCtrl'
                }
            }
        })
        .state('app.schedule', {
            url: '/schedule',
            views: {
                'menuContent': {
                    templateUrl: 'templates/schedule.html',
                    controller: 'ScheduleCtrl'
                }
            }
        })
        .state('app.messages', {
            url: '/messages',
            views: {
                'menuContent': {
                    templateUrl: 'templates/messages.html',
                    controller: 'MessagesCtrl'
                }
            }
        })
        .state('app.mypage', {
            url: '/mypage',
            views: {
                'menuContent': {
                    templateUrl: 'templates/mypage.html',
                    controller: 'MypageCtrl'
                }
            }
        })

        .state('app.tourdetails', {
            url: '/tourdetails/:id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/tourdetails.html',
                    controller: 'TourDetailsCtrl'
                }
            }
        })


        .state('app.tours', {
            url: '/tours',
            views: {
                'menuContent': {
                    templateUrl: 'templates/tours.html',
                    controller: 'DaytripsCtrl'
                }
            }
        })



    ;
        // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/mypage')

    $httpProvider.interceptors.push(function ($rootScope) {
        return {
            request: function (config) {
                if ($rootScope.authToken){
                     config.headers['x-access-token'] = $rootScope.authToken;
                }
                $rootScope.$broadcast('loading:show');
                return config;
            },
            response: function (response) {
                $rootScope.$broadcast('loading:hide');
                return response;
            },
            responseError: function (rejection) { // get back to login in 401 - have to use injector due to circular dependencies
                console.log("INTERECEPTOR SCREWS US")
                if (rejection.status === 401) {
                    $rootScope.$broadcast('unauthorized');
                }

            }
        }
    });


});
