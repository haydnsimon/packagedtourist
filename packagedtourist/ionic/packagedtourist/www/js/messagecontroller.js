    angular.module('packagedtourist.messagecontroller', [])

    .controller('MessagesCtrl', ['messageService', '$scope', '$state', '$ionicPopup', '$ionicModal', 'baseURL', '$ionicPlatform', '$cordovaLocalNotification', '$cordovaToast',
      function(messageService, $scope, $state, $ionicPopup, $ionicModal, baseURL, $ionicPlatform, $cordovaLocalNotification, $cordovaToast) {
        $scope.message = {};
        $scope.message.message = "";
        var services = messageService.resource();
        var servicesInd = messageService.resourceInd();
        var servicesPut = messageService.resourcePut();

        function showMessages() {
          services.query().$promise.then(
            function(messages) {
              // update the messages picture
              for (var i = 0; i < messages.length; i++) {
                if (messages[i].submitted.details.picture) {
                  var temp = messages[i].submitted.details.picture;
                  messages[i].submitted.details.picture = baseURL + temp;
                }
              }
              $scope.messages = messages;
              $scope.showMessages = true;
            },
            function(response) {
              $scope.message = "Error: " + response.status + " " + response.statusText;

            });
        }
        showMessages();
        $scope.confirmDelete = function(messageId) {
          var confirmPopup = $ionicPopup.confirm({
            title: 'Delete Message',
            template: 'Are you sure you want to delete this message?'
          });

          confirmPopup.then(function(res) {
            if (res) {
              servicesInd.remove({
                id: messageId
              }).$promise.then(
                function(response) {
                  showMessages();
                  $ionicPlatform.ready(function() {
                    $cordovaToast
                      .show("Deleted message", 'long', 'center')
                      .then(function(success) {
                        // success
                      }, function(error) {
                        // error
                      });
                  });
                },
                function(response) {
                  $ionicPlatform.ready(function() {
                    $cordovaToast
                      .show("Failed to delete message", 'long', 'center')
                      .then(function(success) {
                        // success
                      }, function(error) {
                        // error
                      });
                  });

                }
              );
            }
          });

        }

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/addmessage.html', {
          scope: $scope
        }).then(function(modal) {
          $scope.modal = modal;
        });


        // Perform the login action when the user submits the login form
        $scope.saveMessage = function(currentMessage) {
          console.log($scope.message);
          var msg = $scope.message;
          if ($scope.message.messageId === null) {
            services.save(msg)
              .$promise.then(
                function(response) {
                  showMessages();
                  $scope.modal.hide();
                  $ionicPlatform.ready(function() {
                    $cordovaToast
                      .show("Added message", 'long', 'center')
                      .then(function(success) {
                        // success
                      }, function(error) {
                        // error
                      });
                  });
                },
                function(response) {
                  console.log("FAILED");
                }
              );
          } else {
            messageService.resourcePut().update(msg, {
                'Id': $scope.message.messageId
              })
              .$promise.then(
                function(response) {
                  console.log("Success");
                  showMessages();
                  $scope.modal.hide();
                },
                function(response) {
                  console.log("Failed");
                }
              );
          }
        };

        $scope.addMessage = function(messageId, messageText) {
          if (messageId) {
            console.log("EDIT");
            $scope.message.message = messageText;
            $scope.message.messageId = messageId;
          } else {
            console.log("NEW");
            $scope.message.messageId = null;
          }
          $scope.modal.show();

        }

      }
    ]);
