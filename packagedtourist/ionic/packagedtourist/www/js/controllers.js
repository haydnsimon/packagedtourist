angular.module('packagedtourist.controllers', [])

.controller('AppCtrl', ['authService', 'messageService', 'tokenHandlerFactory', 'loginFactory', '$rootScope', '$ionicModal', '$localStorage', '$scope', '$window', function(authService, messageService, tokenHandlerFactory, loginFactory, $rootScope, $ionicModal, $localStorage, $scope, $window) {


  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $rootScope.showingLogin = false;
    $scope.modal.hide();
    console.log("Reload page");
    $window.location.reload(true);
  };

  // Open the login modal
  $scope.login = function() {
    $rootScope.showingLogin = true;
    $scope.modal.show();
  };

  $scope.loginData = {};
  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    authService.setUnAuthenticate();
    loginFactory.save($scope.loginData)
      .$promise.then(
        function(response) {
          tokenHandlerFactory.set(response.token);
          $localStorage.store("access_token", response.token);
          authService.setAuthentic();
          $scope.closeLogin();

        },
        function(response) {
          $scope.message = "Error: " + response.status + " " + response.statusText;
          $scope.loginFailed = true;
        }
      );
  };

  authService.testLogin(function handleResult(res) {
    if (res === false) {
      if ($scope.modal) {
        $scope.login();
      } else {
        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
          scope: $scope
        }).then(function(modal) {
          $scope.modal = modal;
          $scope.login();
        });
      }
    }
  });


}])


.controller('MypageCtrl', ['touristService', 'baseURL', '$scope', function(touristService, baseURL, $scope) {
  $scope.tourist = null;
  $scope.details = null;
  $scope.roomPic = null;
  $scope.$on('userReady', function(event, user) {
    touristService.getTourist().get({
        id: user._id
      })
      .$promise.then(
        function(response) {
          $scope.tourist = response;
          $scope.details = response.details;
          $scope.roomPic = baseURL + "images/room" + $scope.tourist.room + ".jpg";
        },
        function(response) {
          console.log("failed to load image");

        }
      );
  });

}])

.controller('StaffCtrl', ['touristService', '$scope', 'baseURL', function(touristService, $scope,baseURL) {
  $scope.staff = null;
  touristService.getStaff().query()
      .$promise.then(
          function (response) {
              for (var i=0 ; i<response.length;i++){
                response[i].pic = baseURL + response[i].details.picture;
                console.log(response[i].pic);
              }
              $scope.staff = response;
          },
          function (response) {
              console.log("failed to get tourist`");

          }
      );

}])


;
