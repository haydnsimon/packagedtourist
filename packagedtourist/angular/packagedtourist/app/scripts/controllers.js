'use strict';

angular.module('packagedTourist.controllers', ['ngDialog'])
    .controller('HeaderCtrl',['baseURL','$scope','$localStorage','$state',function(baseURL,$scope,$localStorage,$state){
      $scope.baseURL = baseURL;
      $scope.logout = function(){
        $localStorage.delete();
        $state.go('app.mypage');
      }
    }])
    .controller('MypageCtrl', ['touristService','baseURL','authService', 'messageService', 'tokenHandlerFactory', 'loginFactory', '$rootScope', 'ngDialog', '$localStorage', '$scope', '$window','$state',
            function(touristService,baseURL,authService,messageService, tokenHandlerFactory, loginFactory, $rootScope, ngDialog, $localStorage, $scope, $window,$state) {
            $scope.baseURL = baseURL;
            $scope.loggedin = false;
            $scope.doLogin = function(loginData){
              ngDialog.close();
            }

            $scope.loginData = {};
            // Perform the login action when the user submits the login form
            $scope.doLogin = function() {
              authService.setUnAuthenticate();
              loginFactory.save($scope.loginData)
                .$promise.then(
                  function(response) {
                    tokenHandlerFactory.set(response.token);
                    $localStorage.store("access_token", response.token);
                    authService.setAuthentic();
                    ngDialog.close();
                    $scope.loggedin = true;
                    $window.location.reload();

                  },
                  function(response) {
                    $scope.message = "Error: " + response.status + " " + response.statusText;
                    $scope.loginFailed = true;
                  }
                );
            };

            authService.testLogin(function handleResult(res) {
               if (res === false) {
                   // Create the login modal that we will use later
                   ngDialog.open({ template: '/views/login.html', className: 'ngdialog-theme-default',  scope: $scope });
                 } else {
                   $scope.loggedin = true;
                 }
              }
             );

        $scope.rtourist = {};
        $scope.rtourist.username = null;
        $scope.rtourist.firstname = null;
        $scope.rtourist.secondname = null;
        $scope.rtourist.password = null;
        $scope.rtourist.telephone= null;
        $scope.rtourist.facebook = null;
        $scope.rtourist.email=null;
        $scope.rtourist.telephone=null;
        $scope.rtourist.age=22;
        $scope.rtourist.gender="M";
        $scope.rtourist.room=12;
        $scope.rtourist.table=11;
        $scope.failRegister = false;

        $scope.register = function(){

          console.log($scope.rtourist.username)
          var promises = touristService.register().save($scope.rtourist).$promise;
          promises.then(
              function (response) {
              },
              function (response) {
                  $scope.failRegister = true;
              }
          );
        }
        $scope.tourist = null;
        $scope.details = null;
        $scope.roomPic = null;
        $scope.staff = null;

        $scope.$on('userReady',function(event,user){
            touristService.getTourist().get({
                    id: user._id
                })
                .$promise.then(
                    function (response) {

                        $scope.tourist = response;
                        $scope.details = response.details;
                        $scope.roomPic = baseURL + "images/room" + $scope.tourist.room + ".jpg";
                    },
                    function (response) {
                        console.log("failed to get tourist`");

                    }
                );
                touristService.getStaff().query()
                    .$promise.then(
                        function (response) {
                            for (var i=0 ; i<response.length;i++){
                              response[i].pic = baseURL + response[i].details.picture;
                              console.log(response[i].pic);
                            }
                            $scope.staff = response;
                        },
                        function (response) {
                            console.log("failed to get tourist`");

                        }
                    );



        });

      }])
;
