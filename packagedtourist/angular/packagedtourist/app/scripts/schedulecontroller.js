'use strict';

angular.module('packagedtourist.schedulecontroller', [])

.controller('ScheduleCtrl', ['spinnerService', 'scheduleService', '$scope', 'baseURL', '$state',function(spinnerService, scheduleService, $scope, baseURL,$state) {

    $scope.schedule = [];
    $scope.dayOptions = [];
    $scope.daySelected = 1;
    $scope.day = {};

    $scope.getSchedule = function(day){
      spinnerService.show('scheduleSpinner');
      console.log(day);
      scheduleService.getScheduleForDay().get({
          dayNo: day,
          nodays : 3
        })
        .$promise.then(
          function(response) {
            $scope.dayOptions=[];
            $scope.schedule=[];
            $scope.maxDay = response.maxDay;
            for (var i=1;i<=response.maxDay;i++){
              $scope.dayOptions.push(i);
            }
            for (var i=0; i<response.events.length; i++){
              if (i==0){
                $scope.day = response.events[i].startTime;
              }
              $scope.schedule.push(response.events[i]);
            }
            spinnerService.hide('scheduleSpinner');
          },
          function(response) {
            console.log("failed to load image");
            spinnerService.hide('scheduleSpinner');
          }
        );
    }
    // listen for the event in the relevant $scope
    $scope.$on('unauthorized', function(event, data) {
        $state.go('app.mypage');
    });

    $scope.getResource = function() {
        $scope.getSchedule(1);
    }

  }])
;
