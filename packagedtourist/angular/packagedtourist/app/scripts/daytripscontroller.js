'use strict';

angular.module('packagedtourist.daytripscontroller', [])

.controller('DaytripsCtrl', ['ngDialog','daytripsService', '$scope', 'baseURL','$state', function(ngDialog,daytripsService, $scope, baseURL,$state) {

        $scope.daytrips;

        function getTrips() {
            daytripsService.getTrips().query().$promise.then(
                function(trips) {
                    for (var i = 0; i < trips.length; i++) {
                        if (trips[i].general.image) {
                            var temp = trips[i].general.image;
                            trips[i].general.image = baseURL + temp;
                        }
                    }
                    $scope.trips = trips;
                },
                function(response) {
                    console.log(response);
                }
            );
        }
        $scope.buyTour = function(tourId) {
            ngDialog.openConfirm({
                template: '\
              <p>Are you sure you want charge your credit card for this trip ?</p>\
              <div class="ngdialog-buttons">\
                  <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                  <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(1)">Yes</button>\
              </div>',
                plain: true
            }).then(function(success) {
                daytripsService.buyTrip().update({
                    'id': tourId
                }, {
                  'id': tourId
                }).$promise.then(
                    function(response) {
                        console.log("Success");
                        getTrips();
                    },
                    function(response) {
                        console.log("Failed");
                    }
                );
            }, function(error) {
                console.log('ok')
            });;
        }

        $scope.$on('unauthorized', function(event, data) {
            $state.go('app.mypage');
        });

        getTrips();

    }])
    .controller('TourDetailsCtrl', ['ngDialog', 'daytripsService', '$scope', 'baseURL', '$stateParams','$state', function(ngDialog, daytripService, $scope, baseURL, $stateParams,$state) {
        // listen for the event in the relevant $scope
        $scope.$on('unauthorized', function(event, data) {
            $state.go('app.mypage');
        });


        $scope.baseURL = baseURL;
        $scope.trip = {};

        $scope.buyTour = function(tourId) {
            /*
                  var confirmPopup = $ionicPopup.confirm({
                    title: 'Buy',
                    template: 'Order this great tour. Will charge your creadit card?'
                  });

                  confirmPopup.then(function(res) {
                    if (res) {
                      daytripService.buyTrip().update({
                        'id': $stateParams.id
                      }, {
                        'id': $stateParams.id
                      }).$promise.then(
                        function(response) {
                          console.log("Success");
                          showMessages();
                        },
                        function(response) {
                          console.log("Failed");
                        }
                      );
                    }
                  });
                */
        }

        daytripService.getTrip().get({
                id: $stateParams.id
            })
            .$promise.then(
                function(response) {
                    $scope.trip = response;
                    console.log(response.general.name)
                    $scope.showTrip = true;
                },
                function(response) {
                    console.log("failed to load image");

                }
            );


    }]);
