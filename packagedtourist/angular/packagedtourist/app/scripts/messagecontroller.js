'use strict';

angular.module('packagedtourist.messagecontroller', [])
    .controller('MessagesCtrl', ['messageService', '$scope', '$state', 'baseURL', 'ngDialog', 'spinnerService', function(messageService, $scope, $state, baseURL, ngDialog, spinnerService) {
        $scope.message = {};
        $scope.message.message = "";
        var services = messageService.resource();
        var servicesInd = messageService.resourceInd();
        var servicesPut = messageService.resourcePut();

        function showMessages() {
            spinnerService.show('mySpinner');
            services.query().$promise.then(
                function(messages) {
                    // update the messages picture
                    for (var i = 0; i < messages.length; i++) {
                        if (messages[i].submitted.details.picture) {
                            var temp = messages[i].submitted.details.picture;
                            messages[i].submitted.details.picture = baseURL + temp;
                        }
                    }
                    $scope.messages = messages;
                    $scope.showMessages = true;
                    spinnerService.hide('mySpinner');
                },
                function(response) {
                    $scope.message = "Error: " + response.status + " " + response.statusText;

                });
        }

        // listen for the event in the relevant $scope
        $scope.$on('unauthorized', function(event, data) {
            $state.go('app.mypage');
        });

        $scope.confirmDelete = function(messageId) {
            ngDialog.openConfirm({
                template: '\
                    <p>Are you sure you want to delete this message ?</p>\
                    <div class="ngdialog-buttons">\
                        <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                        <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(1)">Yes</button>\
                    </div>',
                plain: true
            }).then(function(success) {
                servicesInd.remove({
                    id: messageId
                }).$promise.then(
                    function(response) {
                        console.log("Success");
                        showMessages();
                    },
                    function(response) {
                        console.log("Failed");
                    }
                );
            }, function(error) {
                console.log('ok')
            });;
        }

        $scope.edit = false;

        $scope.showAddMessage = function(messageId, messageText) {
            if (messageId) {
                $scope.edit = true;
                $scope.message.message = messageText;
                $scope.message.messageId = messageId;
            } else {
               $scope.edit = false;
               $scope.message.messageId = null;
            }
            ngDialog.open({
                template: '/views/addMessageTemplate.html',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        }

        $scope.cancelAdd = function(){
            ngDialog.close();
        }

        // Perform the login action when the user submits the login form
        $scope.saveMessage = function(currentMessage) {

            console.log($scope.message);
            var msg = $scope.message;
            console.log("MESSAGE " + $scope.message.messageId);
            if (!$scope.message.messageId) {
                console.log("SAVE");
                services.save(msg)
                    .$promise.then(
                        function(response) {
                            showMessages();
                            ngDialog.close();
                        },
                        function(response) {
                            console.log("FAILED");
                        }
                    );
            } else {
                messageService.resourcePut().update(msg, {
                        'Id': $scope.message.messageId
                    })
                    .$promise.then(
                        function(response) {
                            console.log("Success");
                            showMessages();
                            ngDialog.close();
                        },
                        function(response) {
                            console.log("Failed");
                        }
                    );
            }
        };
        $scope.getResource = function() {
            showMessages();
        }



    }]);
