'use strict';

angular.module('packagedTourist', ['ui.router','angular.filter','ngResource','packagedtourist.schedulecontroller','packagedTourist.controllers','packagedtourist.services','packagedtourist.messagecontroller','packagedtourist.daytripscontroller','angularSpinners'])
.config(function($stateProvider, $urlRouterProvider,$httpProvider) {
        $stateProvider

            // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                    },
                    'content': {
                        templateUrl : 'views/mypage.html',
                        controller  : 'MypageCtrl'
                    }
                }

            })

            // route for the mypage page
            .state('app.messages', {
                url:'messages',
                views: {
                    'content@': {
                        templateUrl : 'views/messages.html',
                        controller  : 'MessagesCtrl'
                    }
                }
            })

            // route for the tours page
            .state('app.tours', {
                url:'tours',
                views: {
                    'content@': {
                        templateUrl : 'views/tours.html',
                        controller  : 'DaytripsCtrl'
                    }
                }
            })

            // route for the schedule page
            .state('app.schedule', {
                url:'schedule',
                views: {
                    'content@': {
                        templateUrl : 'views/schedule.html',
                        controller  : 'ScheduleCtrl'
                    }
                }
            })

            // route for the mypage page
            .state('app.mypage', {
                url:'mypage',
                views: {
                    'content@': {
                        templateUrl : 'views/mypage.html',
                        controller  : 'MypageCtrl'
                    }
                }
            })
            ;

        $urlRouterProvider.otherwise('/');

        $httpProvider.interceptors.push(function ($rootScope) {
            return {
                request: function (config) {
                    if ($rootScope.authToken){
                         config.headers['x-access-token'] = $rootScope.authToken;
                    }
                    $rootScope.$broadcast('loading:show');
                    return config;
                },
                response: function (response) {
                    $rootScope.$broadcast('loading:hide');
                    return response;
                },
                responseError: function (rejection) { // get back to login in 401 - have to use injector due to circular dependencies
                    if (rejection.status >= 400 && rejection.status < 404){
                        $rootScope.$broadcast('unauthorized');
                      }
                }
            }
        });

    })
;
